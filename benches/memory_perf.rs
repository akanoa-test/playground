use iai::black_box;
use playground::buffers::dynamic::BufferDynamic;
use playground::buffers::preallocated::BufferPreallocated;
use playground::seeder::{source_data, SeederConfig};

fn iai_dynamic_allocation() {
    let config = SeederConfig::new(10, 30, 2, 4, 4, 10, false);
    source_data(&config, 42, 1024, |source| {
        let mut save_buffer = BufferDynamic::new();
        let mut work_buffer = BufferDynamic::new();

        playground::parsers::parse(
            black_box(source),
            black_box(&mut save_buffer),
            black_box(&mut work_buffer),
        )
        .unwrap();
    });
}

fn iai_preallocated() {
    let config = SeederConfig::new(10, 30, 2, 4, 4, 10, false);
    source_data(&config, 42, 1024, |source| {
        let mut save_buffer = BufferPreallocated::new(1024);
        let mut work_buffer = BufferPreallocated::new(1024);
        playground::parsers::parse(
            black_box(source),
            black_box(&mut save_buffer),
            black_box(&mut work_buffer),
        )
        .unwrap();
    });
}

fn iai_dynamic_allocation_big_data() {
    let config = SeederConfig::new(1000, 30, 2, 4, 40, 100, false);
    source_data(&config, 42, 1024, |source| {
        let mut save_buffer = BufferDynamic::new();
        let mut work_buffer = BufferDynamic::new();

        playground::parsers::parse(
            black_box(source),
            black_box(&mut save_buffer),
            black_box(&mut work_buffer),
        )
        .unwrap();
    });
}

fn iai_preallocated_big_data() {
    let config = SeederConfig::new(1000, 30, 2, 4, 40, 100, false);
    source_data(&config, 42, 1024, |source| {
        let mut save_buffer = BufferPreallocated::new(4096);
        let mut work_buffer = BufferPreallocated::new(4096);
        playground::parsers::parse(
            black_box(source),
            black_box(&mut save_buffer),
            black_box(&mut work_buffer),
        )
        .unwrap();
    });
}

iai::main!(
    iai_dynamic_allocation,
    iai_preallocated,
    iai_dynamic_allocation_big_data,
    iai_preallocated_big_data
);
