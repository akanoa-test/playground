use bench_macros::generate_bench;
use criterion::{black_box, criterion_group, criterion_main, Criterion};
use playground::seeder::source_data;
use playground::seeder::SeederConfig;

generate_bench!(
    name = big_data;
    config = SeederConfig::new(1400, 30, 2, 4, 4, 1000, false);
    seed = 42;
    parser = playground::parsers::parser_with_buffer::parse;
    buffer = playground::buffers::preallocated::BufferPreallocated::new(1_048_576);
    chunk_sizes = 4096
);

generate_bench!(
    name = small_data;
    config = SeederConfig::new(14, 30, 2, 4, 4, 10, false);
    seed = 42;
    parser = playground::parsers::parser_with_buffer::parse;
    buffer = playground::buffers::preallocated::BufferPreallocated::new(1_048_576);
    chunk_sizes = 4096
);

criterion_main!(small_data, big_data);
