#![no_main]

use libfuzzer_sys::fuzz_target;
use playground::buffers::dynamic::BufferDynamic;
use playground::seeder::generate_groups_data;
use playground::source::Source;
use rand::{Rng, SeedableRng};
use rand_chacha::ChaCha8Rng;

fuzz_target!(|seed: u64| {
    let mut rng = ChaCha8Rng::seed_from_u64(seed);

    let (data_to_parse, expected) = generate_groups_data(&mut rng, 10, 30, 2, 4, 4, 10, false);

    let chunk_size = rng.gen_range(4..1024) as usize;
    let source = Source::new(&data_to_parse).with_chunk_size(chunk_size);

    let mut save_buffer = BufferDynamic::new();
    let mut work_buffer = BufferDynamic::new();

    let parse_result =
        playground::parsers::parser_with_buffer::parse(source, &mut save_buffer, &mut work_buffer);
    assert_eq!(parse_result.unwrap(), expected);
});
