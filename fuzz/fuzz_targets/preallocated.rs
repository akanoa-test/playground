#![no_main]

use libfuzzer_sys::fuzz_target;
use playground::buffers::preallocated::BufferPreallocated;
use playground::seeder::SeederConfig;
use playground::source::Source;
use rand::SeedableRng;
use rand_chacha::ChaCha8Rng;

fuzz_target!(|seed: u64| {
    let mut rng = ChaCha8Rng::seed_from_u64(seed);

    let config = SeederConfig::new(1400, 30, 2, 4, 4, 1000, false);
    let (data_to_parse, expected) = config.generate(&mut rng);

    let source = Source::new(&data_to_parse).with_chunk_size(4096);

    let mut save_buffer = BufferPreallocated::new(1_048_576);
    let mut work_buffer = BufferPreallocated::new(1_048_576);

    let parse_result =
        playground::parsers::parser_with_buffer::parse(source, &mut save_buffer, &mut work_buffer);
    assert_eq!(parse_result.unwrap(), expected);
});
