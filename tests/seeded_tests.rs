#![feature(ascii_char)]

use playground::buffers::preallocated::BufferPreallocated;
use playground::seeder::SeederConfig;
use playground::source::Source;
use playground::{debug, seeder};
use rand::{Rng, SeedableRng};
use rand_chacha::ChaCha8Rng;

#[test]
fn fix_hanako_seed_985264187() {
    let mut rng = ChaCha8Rng::seed_from_u64(985264187);

    let (data_to_parse, expected) =
        seeder::generate_groups_data(&mut rng, 10, 30, 2, 4, 4, 10, false);
    dbg!(debug!(&data_to_parse));

    let chunk_size = rng.gen_range(4..1024) as usize;
    let source = Source::new(&data_to_parse).with_chunk_size(chunk_size);

    let mut save_buffer = BufferPreallocated::new(1_048_576);
    let mut work_buffer = BufferPreallocated::new(1_048_576);

    let parse_result = playground::parsers::parse(source, &mut save_buffer, &mut work_buffer);
    assert_eq!(parse_result.unwrap(), expected);
}

#[test]
fn fix_hanako_seed_86974654131687() {
    let mut rng = ChaCha8Rng::seed_from_u64(86974654131687);

    let (data_to_parse, expected) =
        seeder::generate_groups_data(&mut rng, 10, 30, 2, 4, 4, 10, false);
    dbg!(debug!(&data_to_parse));

    let chunk_size = rng.gen_range(4..1024) as usize;
    let source = Source::new(&data_to_parse).with_chunk_size(chunk_size);

    let mut save_buffer = BufferPreallocated::new(1_048_576);
    let mut work_buffer = BufferPreallocated::new(1_048_576);

    let parse_result = playground::parsers::parse(source, &mut save_buffer, &mut work_buffer);
    assert_eq!(parse_result.unwrap(), expected);
}

#[test]
fn fix_hanako_seed_696874460166() {
    //Logs::new().color(true).init();

    let mut rng = ChaCha8Rng::seed_from_u64(696874460166);

    let (data_to_parse, expected) =
        seeder::generate_groups_data(&mut rng, 10, 30, 2, 4, 4, 10, false);
    dbg!(debug!(&data_to_parse));

    let chunk_size = rng.gen_range(4..1024) as usize;
    let source = Source::new(&data_to_parse).with_chunk_size(chunk_size);

    let mut save_buffer = BufferPreallocated::new(1_048_576);
    let mut work_buffer = BufferPreallocated::new(1_048_576);

    let parse_result = playground::parsers::parse(source, &mut save_buffer, &mut work_buffer);
    assert_eq!(parse_result.unwrap(), expected);
}

#[test]
fn fix_hanako_seed_168() {
    let mut rng = ChaCha8Rng::seed_from_u64(168);

    let (data_to_parse, expected) =
        seeder::generate_groups_data(&mut rng, 10, 30, 2, 4, 4, 10, false);
    dbg!(debug!(&data_to_parse));

    let chunk_size = rng.gen_range(4..1024) as usize;
    let source = Source::new(&data_to_parse).with_chunk_size(chunk_size);

    let mut save_buffer = BufferPreallocated::new(1_048_576);
    let mut work_buffer = BufferPreallocated::new(1_048_576);

    let parse_result = playground::parsers::parse(source, &mut save_buffer, &mut work_buffer);
    assert_eq!(parse_result.unwrap(), expected);
}

#[test]
fn fix_hanako_seed_18446743176212973962() {
    let mut rng = ChaCha8Rng::seed_from_u64(18446743176212973962);

    let (data_to_parse, expected) =
        seeder::generate_groups_data(&mut rng, 10, 30, 2, 4, 4, 10, false);
    dbg!(debug!(&data_to_parse));

    let chunk_size = rng.gen_range(4..1024) as usize;
    let source = Source::new(&data_to_parse).with_chunk_size(chunk_size);

    let mut save_buffer = BufferPreallocated::new(1_048_576);
    let mut work_buffer = BufferPreallocated::new(1_048_576);

    let parse_result = playground::parsers::parse(source, &mut save_buffer, &mut work_buffer);
    assert_eq!(parse_result.unwrap(), expected);
}

#[test]
fn fix_hanako_seed_17799245164314167561() {
    let mut rng = ChaCha8Rng::seed_from_u64(17799245164314167561);

    let (data_to_parse, expected) =
        seeder::generate_groups_data(&mut rng, 10, 30, 50, 50, 4, 10, false);
    dbg!(debug!(&data_to_parse));

    let chunk_size = rng.gen_range(4..1024) as usize;
    let source = Source::new(&data_to_parse).with_chunk_size(chunk_size);

    let mut save_buffer = BufferPreallocated::new(1_048_576);
    let mut work_buffer = BufferPreallocated::new(1_048_576);

    let parse_result = playground::parsers::parse(source, &mut save_buffer, &mut work_buffer);
    assert_eq!(parse_result.unwrap(), expected);
}

#[test]
fn fix_hanako_seed_579570213918935601() {
    let mut rng = ChaCha8Rng::seed_from_u64(579570213918935601);

    let (data_to_parse, expected) =
        seeder::generate_groups_data(&mut rng, 10, 30, 2, 4, 4, 10, true);
    dbg!(debug!(&data_to_parse));

    let chunk_size = rng.gen_range(4..1024) as usize;
    let source = Source::new(&data_to_parse).with_chunk_size(chunk_size);

    let mut save_buffer = BufferPreallocated::new(1_048_576);
    let mut work_buffer = BufferPreallocated::new(1_048_576);

    let parse_result = playground::parsers::parse(source, &mut save_buffer, &mut work_buffer);

    assert_eq!(parse_result.unwrap(), expected);
}

#[test_pretty_log::test]
fn fix_hanako_seed_3170305434959044791() {
    //Logs::new().color(true).init();

    let mut rng = ChaCha8Rng::seed_from_u64(3170305434959044791);

    let (data_to_parse, expected) =
        seeder::generate_groups_data(&mut rng, 10, 30, 2, 4, 4, 10, true);
    dbg!(debug!(&data_to_parse));

    let chunk_size = rng.gen_range(4..1024) as usize;
    let source = Source::new(&data_to_parse).with_chunk_size(chunk_size);

    let mut save_buffer = BufferPreallocated::new(1_048_576);
    let mut work_buffer = BufferPreallocated::new(1_048_576);

    let parse_result = playground::parsers::parse(source, &mut save_buffer, &mut work_buffer);
    assert_eq!(parse_result.unwrap(), expected);
}

#[test]
fn fix_hanako_seed_651614571940806655() {
    let mut rng = ChaCha8Rng::seed_from_u64(651614571940806655);

    let (data_to_parse, expected) =
        seeder::generate_groups_data(&mut rng, 10, 30, 2, 4, 4, 10, false);
    dbg!(debug!(&data_to_parse));

    let chunk_size = rng.gen_range(4..1024) as usize;
    let source = Source::new(&data_to_parse).with_chunk_size(chunk_size);

    let mut save_buffer = BufferPreallocated::new(1_048_576);
    let mut work_buffer = BufferPreallocated::new(1_048_576);

    let parse_result = playground::parsers::parse(source, &mut save_buffer, &mut work_buffer);
    assert_eq!(parse_result.unwrap(), expected);
}

#[test]
fn fix_hanako_seed_18374871197390209081() {
    let mut rng = ChaCha8Rng::seed_from_u64(18374871197390209081);

    let (data_to_parse, expected) =
        seeder::generate_groups_data(&mut rng, 10, 30, 2, 4, 4, 10, false);
    dbg!(debug!(&data_to_parse));

    let chunk_size = rng.gen_range(4..1024) as usize;
    let source = Source::new(&data_to_parse).with_chunk_size(chunk_size);

    let mut save_buffer = BufferPreallocated::new(1_048_576);
    let mut work_buffer = BufferPreallocated::new(1_048_576);

    let parse_result = playground::parsers::parse(source, &mut save_buffer, &mut work_buffer);
    assert_eq!(parse_result.unwrap(), expected);
}

#[test]
fn fix_hanako_seed_4413810248126119405() {
    //Logs::new().color(true).init();

    let mut rng = ChaCha8Rng::seed_from_u64(4413810248126119405);

    let (data_to_parse, expected) =
        seeder::generate_groups_data(&mut rng, 10, 30, 2, 4, 4, 10, false);
    dbg!(debug!(&data_to_parse));

    let chunk_size = rng.gen_range(4..1024) as usize;
    let source = Source::new(&data_to_parse).with_chunk_size(chunk_size);

    let mut save_buffer = BufferPreallocated::new(1_048_576);
    let mut work_buffer = BufferPreallocated::new(1_048_576);

    let parse_result = playground::parsers::parse(source, &mut save_buffer, &mut work_buffer);
    assert_eq!(parse_result.unwrap(), expected);
}

#[test]
fn fix_hanako_seed_18377219788973345033() {
    let mut rng = ChaCha8Rng::seed_from_u64(18377219788973345033);

    let (data_to_parse, expected) =
        seeder::generate_groups_data(&mut rng, 10, 30, 2, 4, 4, 10, true);
    dbg!(debug!(&data_to_parse));
    println!("{:?}", &expected);

    let chunk_size = rng.gen_range(4..1024) as usize;
    let source = Source::new(&data_to_parse).with_chunk_size(chunk_size);

    let mut save_buffer = BufferPreallocated::new(1_048_576);
    let mut work_buffer = BufferPreallocated::new(1_048_576);

    let parse_result = playground::parsers::parse(source, &mut save_buffer, &mut work_buffer);
    assert_eq!(parse_result.unwrap(), expected);
}

#[test]
fn fix_failed_seed_9982943851645504003() {
    let mut rng = ChaCha8Rng::seed_from_u64(9982943851645504003);

    let (data_to_parse, expected) =
        seeder::generate_groups_data(&mut rng, 10, 30, 2, 4, 4, 10, true);
    dbg!(debug!(&data_to_parse));
    println!("{:?}", &expected);

    let chunk_size = rng.gen_range(4..1024) as usize;
    let source = Source::new(&data_to_parse).with_chunk_size(chunk_size);

    let mut save_buffer = BufferPreallocated::new(1_048_576);
    let mut work_buffer = BufferPreallocated::new(1_048_576);

    let parse_result = playground::parsers::parse(source, &mut save_buffer, &mut work_buffer);
    assert_eq!(parse_result.unwrap(), expected);
}

#[test]
fn fix_failed_seed_218706739814408969() {
    let mut rng = ChaCha8Rng::seed_from_u64(218706739814408969);

    let (data_to_parse, expected) =
        seeder::generate_groups_data(&mut rng, 10, 30, 2, 4, 4, 10, true);
    dbg!(debug!(&data_to_parse));
    println!("{:?}", &expected);

    let chunk_size = rng.gen_range(4..1024) as usize;
    let source = Source::new(&data_to_parse).with_chunk_size(chunk_size);

    let mut save_buffer = BufferPreallocated::new(1_048_576);
    let mut work_buffer = BufferPreallocated::new(1_048_576);

    let parse_result = playground::parsers::parse(source, &mut save_buffer, &mut work_buffer);
    assert_eq!(parse_result.unwrap(), expected);
}

#[test]
fn test_previous_failed_seeds() {
    let failed_seeds: Vec<u64> = vec![
        651614571940806655,
        18374871197390209081,
        4413810248126119405,
        18377219788973345033,
        9982943851645504003,
        218706739814408969,
    ];

    for failed_seed in failed_seeds {
        dbg!(failed_seed);

        let mut rng = ChaCha8Rng::seed_from_u64(failed_seed);

        let (data_to_parse, expected) =
            seeder::generate_groups_data(&mut rng, 10, 30, 2, 4, 4, 10, true);
        dbg!(debug!(&data_to_parse));

        let chunk_size = rng.gen_range(4..1024) as usize;
        let source = Source::new(&data_to_parse).with_chunk_size(chunk_size);

        let mut save_buffer = BufferPreallocated::new(1_048_576);
        let mut work_buffer = BufferPreallocated::new(1_048_576);

        let parse_result = playground::parsers::parse(source, &mut save_buffer, &mut work_buffer);
        assert_eq!(parse_result.unwrap(), expected);
    }
}

#[test]
fn test_generate_data() {
    let hanako_seeds: Vec<u64> = vec![
        5774267489,
        985264187,
        325687496,
        41682321698,
        15679864684,
        327151782,
        435214501564,
        564189410462,
        754161569874,
        6549825654,
        86974654131687,
        3512005267489,
        1489641510065,
        56415051,
        6846,
        7,
        8008,
        748525168,
        2056498641,
        16874986,
        564165386,
        168498641501,
        18694799864210,
        696874460166,
        165847986741,
        6541649864163,
        16546841,
        1654168746315684,
        6848694651541684,
        3213216546984,
        6,
        561568914185,
        1869418514,
        85186419841,
        1869484,
        1684984,
        141894,
        84,
        6941189649,
        148694,
        14984,
        418498498641651,
        8914894984,
        1498498411654,
        4918541698,
        84984941,
        564164984984185419,
        9841986498465418694,
        86484854,
        8498874968,
        18446743176212973962,
        168,
        17799245164314167561,
        579570213918935601,
        5216562654698,
        3170305434959044791,
    ];

    for hanako_seed in hanako_seeds {
        dbg!(hanako_seed);

        let mut rng = ChaCha8Rng::seed_from_u64(hanako_seed);

        let (data_to_parse, expected) =
            seeder::generate_groups_data(&mut rng, 10, 30, 2, 4, 4, 10, true);
        dbg!(debug!(&data_to_parse));

        let chunk_size = rng.gen_range(4..1024) as usize;
        let source = Source::new(&data_to_parse).with_chunk_size(chunk_size);

        let mut save_buffer = BufferPreallocated::new(1_048_576);
        let mut work_buffer = BufferPreallocated::new(1_048_576);

        let parse_result = playground::parsers::parse(source, &mut save_buffer, &mut work_buffer);
        assert_eq!(parse_result.unwrap(), expected);
    }
}

#[test]
fn test_failed_seed_7926503122928874368() {
    let mut rng = ChaCha8Rng::seed_from_u64(7926503122928874368);

    let config = SeederConfig::new(1400, 30, 2, 4, 4, 1000, false);
    let (data_to_parse, expected) = config.generate(&mut rng);

    let source = Source::new(&data_to_parse).with_chunk_size(4096);

    let mut save_buffer = BufferPreallocated::new(1_048_576);
    let mut work_buffer = BufferPreallocated::new(1_048_576);

    let parse_result = playground::parsers::parse(source, &mut save_buffer, &mut work_buffer);
    assert_eq!(parse_result.unwrap(), expected);
}
