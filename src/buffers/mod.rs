use crate::ExceedBuffer;
use std::ops::Deref;

pub mod dynamic;
pub mod preallocated;

pub trait Buffer: Deref<Target = [u8]> {
    fn append(&mut self, other: &[u8], evinceable: Option<usize>) -> Result<bool, ExceedBuffer>;
    fn copy_from(&mut self, source: &Self, evinceable: Option<usize>);
    fn clear(&mut self);
}
