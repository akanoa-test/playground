#![feature(ascii_char)]

pub mod parsers;
pub mod perf;
pub mod seeder;
pub mod source;

#[macro_export]
macro_rules! debug {
    ($input:expr) => {
        String::from_utf8_lossy($input)
    };
}

#[derive(Debug, PartialEq)]
pub struct ExceedBuffer;

pub mod buffers;

#[cfg(test)]
mod tests {}
