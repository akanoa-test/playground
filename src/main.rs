//ttt(1,2,3,4)|(4,8,15,35,8)(4,36,a,9,45)(69,4)###
//^   ^                           ^          ^
//|   |

// use crabgrind as cg;
use playground::buffers::preallocated::BufferPreallocated;
use playground::buffers::Buffer;
use playground::seeder::{source_data, SeederConfig};
use playground::source::Source;

fn main() {
    let config = SeederConfig::new(1000, 30, 2, 4, 40, 100, false);
    source_data(&config, 42, 1024, |source| {
        let mut save_buffer = BufferPreallocated::new(4096);
        let mut work_buffer = BufferPreallocated::new(4096);
        run(source, &mut save_buffer, &mut work_buffer);
    });
}

fn run<B: Buffer>(source: Source, save_buffer: &mut B, work_buffer: &mut B) {
    playground::parsers::parse(source, save_buffer, work_buffer).unwrap();
}
