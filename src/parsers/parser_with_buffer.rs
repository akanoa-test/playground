use crate::buffers::Buffer;
use crate::parsers::parse_data;
use crate::{debug, parsers::SearchState, source::Source, ExceedBuffer};

fn run_iteration_zero_copy<B: Buffer>(
    data: &[u8],
    save_buffer: &mut B,
    work_buffer: &mut B,
    state: &mut SearchState,
    results: &mut Vec<Vec<u8>>,
) -> Result<(), ExceedBuffer> {
    tracing::debug!("Entering iteration");
    tracing::trace!("New data : {}", debug!(data));
    let mut cursor = 0;
    work_buffer.clear();
    work_buffer.copy_from(save_buffer, None);
    work_buffer.append(data, None)?;
    'outer: loop {
        let input = &work_buffer[cursor..];
        if let SearchState::SearchForStart = state {
            tracing::debug!("Search for a new group start");
            tracing::trace!("In {}", debug!(input));
            // On vérifie si le début de groupe est dans le buffer mémoire
            let result_search_for_start = nom::bytes::complete::take_until::<_, _, ()>("(")(input);

            match result_search_for_start {
                Err(_) => {
                    tracing::debug!("No group start found");
                    tracing::trace!("In {}", debug!(input));
                    tracing::debug!("Cleaning safe buffer");
                    save_buffer.clear();
                    tracing::trace!("Asking for more data");
                    *state = SearchState::SearchForStart;
                    break;
                }
                Ok((remain, garbage)) => {
                    tracing::debug!("Found group start");
                    tracing::trace!(
                        "In {} remain = {} garbage = {}",
                        debug!(input),
                        debug!(remain),
                        debug!(garbage)
                    );
                    cursor += garbage.len();
                    *state = SearchState::StartFound
                }
            }
        }

        let mut input = &work_buffer[cursor..];

        let parse_error = loop {
            tracing::debug!("Trying to parse tokens");
            tracing::trace!("Tokens parsed = {}", debug!(input));
            let result_parse = parse_data(input);
            match result_parse {
                Ok((remain, data)) => {
                    tracing::debug!("Successfully parse tokens");
                    tracing::trace!("Found data = {data:?}");
                    tracing::trace!("Remaining token = {}", debug!(remain));
                    cursor += input.len() - remain.len();
                    tracing::trace!(
                        "Shift cursor from {} to {}",
                        debug!(input),
                        debug!(&work_buffer[cursor..])
                    );
                    input = remain;
                    results.push(data)
                }
                Err(err) => {
                    tracing::debug!("Unable to parse tokens");
                    tracing::trace!("Tokens = {}", debug!(input));
                    break err;
                }
            }
        };

        match parse_error {
            nom::Err::Incomplete(_) => {
                tracing::debug!("Not enough data to decide");
                tracing::trace!("In {}", debug!(input));
                tracing::debug!("Asking for more data on incomplete data");
                save_buffer.clear();
                save_buffer.append(input, None)?;
                break 'outer;
            }
            _ => {
                *state = SearchState::SearchForStart;

                if input.is_empty() {
                    break;
                }

                if input[0] == b'(' {
                    cursor += 1;
                }
            }
        }
    }
    tracing::trace!("Leaving iteration");
    Ok(())
}

pub fn parse<B: Buffer>(
    source: Source,
    save_buffer: &mut B,
    work_buffer: &mut B,
) -> Result<Vec<Vec<u8>>, ExceedBuffer> {
    let mut state = SearchState::SearchForStart;
    let mut results: Vec<Vec<u8>> = vec![];

    for data in source {
        run_iteration_zero_copy(data, save_buffer, work_buffer, &mut state, &mut results)?;
    }

    Ok(results)
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::buffers::preallocated::BufferPreallocated;

    #[test_pretty_log::test]
    fn test_parse_source() {
        let data = b"(63,36,16,20,7)(62))(45,18,47,77,0,40,59,21)(21,6)<.(39,4,3)(76,47,83,55,33,5,10,20,28)R(2,63,67,40,57))(14,34)(";
        let source = Source::new(data).with_chunk_size(20);
        let expected = vec![
            vec![63, 36, 16, 20, 7],
            vec![62],
            vec![45, 18, 47, 77, 0, 40, 59, 21],
            vec![21, 6],
            vec![39, 4, 3],
            vec![76, 47, 83, 55, 33, 5, 10, 20, 28],
            vec![2, 63, 67, 40, 57],
            vec![14, 34],
        ];

        let mut save_buffer = BufferPreallocated::new(1024);
        let mut work_buffer = BufferPreallocated::new(1024);

        assert_eq!(
            expected,
            parse(source, &mut save_buffer, &mut work_buffer).unwrap()
        );
    }
}
