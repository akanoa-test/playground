use nom::{
    bytes::streaming::tag, character, character::streaming::digit1, combinator::map_parser,
    multi::separated_list1, sequence::delimited, IResult,
};

pub mod parser_lazy;
pub mod parser_with_buffer;
pub mod stream_parser;

pub use parser_with_buffer::parse;

pub fn parse_digit(input: &[u8]) -> nom::IResult<&[u8], u8> {
    let (remain, result) = map_parser(digit1, character::complete::u8)(input)?;
    Ok((remain, result))
}

pub fn parse_digit_u32(input: &[u8]) -> nom::IResult<&[u8], u32> {
    let (remain, result) = map_parser(digit1, character::complete::u32)(input)?;
    Ok((remain, result))
}

pub fn parse_data(input: &[u8]) -> nom::IResult<&[u8], Vec<u8>> {
    delimited(tag("("), separated_list1(tag(","), parse_digit), tag(")"))(input)
}

pub fn parse_data_u32(input: &[u8]) -> nom::IResult<&[u8], Vec<u32>> {
    delimited(
        tag("%%"),
        separated_list1(tag(","), parse_digit_u32),
        tag("%%"),
    )(input)
}

pub fn parse_percentage(input: &[u8]) -> IResult<&[u8], Vec<u8>> {
    delimited(tag("%%"), separated_list1(tag(";"), parse_digit), tag("%%"))(input)
}

pub fn parse_chinese(input: &[u8]) -> IResult<&[u8], Vec<u8>> {
    delimited(
        tag(b"\xE4\xBD\xA0"),
        separated_list1(tag(","), parse_digit),
        tag(b"\xE5\xA5\xBD"),
    )(input)
}

#[derive(Debug)]
pub enum SearchState {
    SearchForStart,
    // on a pas encore trouvé la parenthèse ouvrante
    StartFound, // la parenthèse ouvrante a été trouvée
}

#[cfg(test)]
mod tests {
    use super::*;
    use nom::AsBytes;

    #[test]
    fn test_parse_digit() {
        let data = b"246a";
        let expected = 246;
        let result = parse_digit(data);
        assert_eq!(result, Ok((b"a".as_bytes(), expected)));
    }

    #[test]
    fn parse_message() {
        let data = b"(1,24,3,4,64)(";
        let expected = vec![1, 24, 3, 4, 64];
        let result = parse_data(data);
        assert_eq!(result, Ok((b"(".as_bytes(), expected)));

        let data = b"(1,24,a,4,64)";
        let result = parse_data(data);
        // bad group
        assert!(result.is_err());

        let data = b"(1,24,";
        let result = parse_data(data);
        dbg!(&result);
        // incomplete
        assert!(result.is_err());
    }
}
