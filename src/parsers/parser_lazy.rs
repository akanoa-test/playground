use crate::buffers::Buffer;
use crate::parsers::stream_parser::PreviousParseState;
use crate::parsers::SearchState;
use crate::{debug, ExceedBuffer};
use nom::Parser;
use std::fmt::Debug;

pub type ParserFunction<R> = fn(&[u8]) -> nom::IResult<&[u8], R>;
pub type ParserFunctionStartGroup = fn(&[u8]) -> nom::IResult<&[u8], &[u8]>;

#[derive(Debug)]
pub enum ReturnState<'a, R> {
    NeedMoreData,
    Data(R),
    Error(nom::Err<nom::error::Error<&'a [u8]>>),
}

pub struct StartGroup<'a> {
    pub parser: ParserFunctionStartGroup,
    pub start_character: &'a [u8],
}

pub fn parse<'a, 'b, B: Buffer, R: Debug>(
    save_buffer: &mut B,
    work_buffer: &'b mut B,
    state: &mut (SearchState, PreviousParseState),
    cursor: &mut usize,
    parser: fn(&'b [u8]) -> Result<(&'b [u8], R), nom::Err<nom::error::Error<&'b [u8]>>>,
    start_group: &'b mut StartGroup<'a>,
) -> Result<ReturnState<'b, R>, ExceedBuffer>
where
{
    let input = &work_buffer[*cursor..];
    if let (SearchState::SearchForStart, _) = state {
        tracing::debug!("Search for a new group start");
        tracing::trace!("In {}", debug!(input));
        // On vérifie si le début de groupe est dans le buffer mémoire
        let result_search_for_start =
            nom::bytes::complete::take_until::<_, _, ()>(start_group.start_character)(input);

        match result_search_for_start {
            Err(_) => {
                tracing::debug!("No group start found");
                tracing::trace!("In {}", debug!(input));
                tracing::debug!("Cleaning buffers");
                save_buffer.clear();
                work_buffer.clear();
                *cursor = 0;
                tracing::trace!("Asking for more data");
                state.0 = SearchState::SearchForStart;
                return Ok(ReturnState::NeedMoreData);
            }
            Ok((remain, garbage)) => {
                tracing::debug!("Found group start");
                tracing::trace!(
                    "In {} remain = {} garbage = {}",
                    debug!(input),
                    debug!(remain),
                    debug!(garbage)
                );

                let result_group_start_complete = start_group.parser.parse(remain);

                match result_group_start_complete {
                    Ok((_remain, garbage2)) => {
                        tracing::debug!("Found complete group start");
                        tracing::trace!(
                            "In {} remain = {} garbage2 = {}",
                            debug!(input),
                            debug!(remain),
                            debug!(garbage2)
                        );
                        *cursor += garbage.len() + garbage2.len();
                        state.0 = SearchState::StartFound
                    }
                    Err(nom::Err::Incomplete(_)) => {
                        return Ok(ReturnState::NeedMoreData);
                    }
                    Err(_) => {
                        tracing::debug!("No group start found");
                        tracing::trace!("In {}", debug!(input));
                        tracing::debug!("Cleaning buffers");
                        save_buffer.clear();
                        work_buffer.clear();
                        *cursor = 0;
                        tracing::trace!("Asking for more data");
                        state.0 = SearchState::SearchForStart;
                        return Ok(ReturnState::NeedMoreData);
                    }
                }
            }
        }
    }

    let input = &work_buffer[*cursor..];

    tracing::debug!("Parsing data {}", debug!(input));

    let result_parse = parser(input);

    match result_parse {
        Ok((remain, data)) => {
            tracing::debug!("Successfully parse tokens");
            tracing::trace!("Found data = {data:?}");
            tracing::trace!("Remaining token = {}", debug!(remain));
            *cursor += input.len() - remain.len();
            tracing::trace!(
                "Shift cursor from {} to {}",
                debug!(input),
                debug!(&work_buffer[*cursor..])
            );
            state.0 = SearchState::SearchForStart;

            if work_buffer[*cursor..].is_empty() {
                save_buffer.clear()
            }

            return Ok(ReturnState::Data(data));
        }
        Err(nom::Err::Incomplete(_)) => {
            tracing::debug!("Not enough data to decide");
            tracing::trace!("In {}", debug!(input));
            tracing::debug!("Asking for more data on incomplete data");
            save_buffer.clear();
            save_buffer.append(input, Some(*cursor))?;
        }
        Err(err) => {
            tracing::trace!(
                "Error in {} to {}",
                debug!(input),
                debug!(&work_buffer[*cursor..])
            );

            state.0 = SearchState::SearchForStart;

            if input.is_empty() {
                return Ok(ReturnState::NeedMoreData);
            }

            let old_input = &work_buffer[*cursor..];

            if let Ok((remain, found)) = nom::combinator::peek(start_group.parser)(input) {
                tracing::trace!("Remain : {}", debug!(remain));
                tracing::trace!("Found : {}", debug!(found));

                *cursor += 1;

                tracing::trace!(
                    "Shift cursor from {} to {}",
                    debug!(old_input),
                    debug!(&work_buffer[*cursor..])
                );
            }

            return Ok(ReturnState::Error(err.map_input(|_x| old_input)));
        }
    }
    Ok(ReturnState::NeedMoreData)
}
