use crate::buffers::Buffer;
use crate::parsers::parser_lazy::{ParserFunction, ReturnState, StartGroup};
use crate::parsers::SearchState;
use crate::source::Source;
use crate::{debug, ExceedBuffer};
use itertools::{unfold, Unfold};
use nom::IResult;
use std::fmt::Debug;
use std::ops::{Deref, DerefMut};

#[derive(Debug)]
pub enum PreviousParseState {
    NeedMoreData,
    MaybeParsable,
}

pub struct LogicState<'a, I, B, R>
where
    I: Iterator<Item = &'a [u8]>,
    B: Buffer,
{
    pub iterator: I,
    pub save_buffer: &'a mut B,
    pub work_buffer: &'a mut B,
    pub state: (SearchState, PreviousParseState),
    pub cursor: usize,
    pub parser: ParserFunction<R>,
    pub start_group: StartGroup<'a>,
    #[allow(unused)]
    /// Used to debug the system when it comes to infinite loop
    i: usize,
}

impl<'a, I, B, R> Deref for LogicState<'a, I, B, R>
where
    B: Buffer,
    I: Iterator<Item = &'a [u8]>,
{
    type Target = I;

    fn deref(&self) -> &Self::Target {
        &self.iterator
    }
}

impl<'a, I, B, R> DerefMut for LogicState<'a, I, B, R>
where
    I: Iterator<Item = &'a [u8]>,
    B: Buffer,
{
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.iterator
    }
}

impl<'a, I, B, R> LogicState<'a, I, B, R>
where
    I: Iterator<Item = &'a [u8]> + Iterator<Item = &'a [u8]>,
    B: Buffer,
{
    fn new(
        save_buffer: &'a mut B,
        work_buffer: &'a mut B,
        iterator: I,
        parser: ParserFunction<R>,
        start_group: StartGroup<'a>,
    ) -> Self {
        Self {
            iterator,
            save_buffer,
            work_buffer,
            state: (
                SearchState::SearchForStart,
                PreviousParseState::NeedMoreData,
            ),
            cursor: 0,
            parser,
            start_group,
            i: 0,
        }
    }
}

type Logic<St, R> =
    Box<dyn FnMut(&mut St) -> Option<Result<R, nom::Err<nom::error::Error<Vec<u8>>>>>>;

type SteamUnfold<'a, I, B, R> = Unfold<LogicState<'a, I, B, R>, Logic<LogicState<'a, I, B, R>, R>>;

pub struct StreamParser<'a, I, B, R>
where
    I: Iterator<Item = &'a [u8]>,
    B: Buffer,
{
    stream: SteamUnfold<'a, I, B, R>,
}
fn logic<'a, I, B, R>() -> Logic<LogicState<'a, I, B, R>, R>
where
    I: Iterator<Item = &'a [u8]>,
    B: Buffer,
    R: Debug,
{
    Box::new(|x: &mut LogicState<'a, I, B, R>| {
        tracing::info!("New next() call");
        tracing::debug!("At next() call state : {:?}", x.state);
        tracing::trace!("Cursor: {}", x.cursor);

        // Eviction de donnée

        loop {
            // x.i += 1;
            // if x.i > 20 {
            //     return None;
            // }

            // We yield more data from source iterator if the previous
            // ask for or if the work_buffer is empty
            let current_len = x.work_buffer[x.cursor..].len();
            if let ((_, PreviousParseState::NeedMoreData), _) | (_, 0) = (&x.state, current_len) {
                tracing::debug!("Asking for more data");

                let data = x.iterator.next();

                if let Some(data) = data {
                    tracing::trace!("New data : {}", debug!(data));
                    let eviction = x.work_buffer.append(data, Some(x.cursor)).unwrap();
                    tracing::debug!("After append work buffer");
                    if eviction {
                        x.cursor = 0;
                    }

                    x.state.1 = PreviousParseState::MaybeParsable;
                } else {
                    if x.save_buffer.is_empty() {
                        tracing::trace!("No more data can be yield and save buffer empty");
                        return None;
                    }

                    tracing::trace!("Save buffer : {}", debug!(x.save_buffer));
                    let peek_start_group_result =
                        nom::combinator::peek(x.start_group.parser)(x.save_buffer);
                    if let Ok((remain, found)) = peek_start_group_result {
                        tracing::trace!("Remain : {}", debug!(remain));
                        tracing::trace!("Found : {}", debug!(found));
                        if found.is_empty() {
                            tracing::trace!("The only data left to parse are an initial group");
                            return None;
                        }
                    }
                }
            }

            tracing::debug!("Parsing work buffer");

            let return_state = crate::parsers::parser_lazy::parse(
                x.save_buffer,
                x.work_buffer,
                &mut x.state,
                &mut x.cursor,
                x.parser,
                &mut x.start_group,
            )
            .unwrap();

            match return_state {
                ReturnState::NeedMoreData => x.state.1 = PreviousParseState::NeedMoreData,
                ReturnState::Data(data) => {
                    x.state.1 = PreviousParseState::MaybeParsable;
                    return Some(Ok(data));
                }
                ReturnState::Error(err) => {
                    tracing::debug!("Yield an error");
                    return Some(Err(err.to_owned()));
                }
            }
        }
    })
}

impl<'a, I, B, R> StreamParser<'a, I, B, R>
where
    I: Iterator<Item = &'a [u8]>,
    B: Buffer,
    R: Debug,
{
    pub fn new(
        iterator: I,
        save_buffer: &'a mut B,
        work_buffer: &'a mut B,
        parser: ParserFunction<R>,
        group_start: StartGroup<'a>,
    ) -> Self {
        let logic_state = LogicState::new(save_buffer, work_buffer, iterator, parser, group_start);

        let stream = unfold(logic_state, logic());
        StreamParser { stream }
    }
}

impl<'a, I, B, R> Iterator for StreamParser<'a, I, B, R>
where
    I: Iterator<Item = &'a [u8]>,
    B: Buffer,
    R: Debug,
{
    type Item = Result<R, nom::Err<nom::error::Error<Vec<u8>>>>;

    fn next(&mut self) -> Option<Self::Item> {
        self.stream.next()
    }
}

fn start_group_parenthesis(input: &[u8]) -> IResult<&[u8], &[u8]> {
    nom::bytes::streaming::take_until("(")(input)
}

pub fn parse<B: Buffer>(
    source: Source,
    save_buffer: &mut B,
    work_buffer: &mut B,
) -> Result<Vec<Vec<u8>>, ExceedBuffer> {
    let parser = crate::parsers::parse_data;
    let group_start = StartGroup {
        parser: start_group_parenthesis,
        start_character: b"(",
    };
    let stream = StreamParser::new(source, save_buffer, work_buffer, parser, group_start);
    let result: Vec<Vec<u8>> = stream.filter_map(|x| x.ok()).collect();
    Ok(result)
}

#[cfg(test)]
mod tests {
    use crate::buffers::preallocated::BufferPreallocated;
    use crate::parsers::parser_lazy::StartGroup;
    use crate::parsers::stream_parser::{parse, start_group_parenthesis, StreamParser};
    use crate::parsers::{parse_chinese, parse_data_u32, parse_percentage};
    use crate::source::Source;
    use nom::IResult;

    fn start_group_percentage(input: &[u8]) -> IResult<&[u8], &[u8]> {
        nom::bytes::streaming::take_until("%%")(input)
    }

    fn start_group_chinese(input: &[u8]) -> IResult<&[u8], &[u8]> {
        nom::bytes::streaming::take_until("你".as_bytes())(input)
    }

    #[test_pretty_log::test]
    fn test_yield_source() {
        let data = b"(62,36,16,20,7)(62))(45,18,47,77,0,40,59,21)(21,6)<.(39,4,3)(76,47,83,55,33,5,10,20,28)R(2,63,67,40,57))(14,34)(";
        let expected = vec![
            vec![62, 36, 16, 20, 7],
            vec![62],
            vec![45, 18, 47, 77, 0, 40, 59, 21],
            vec![21, 6],
            vec![39, 4, 3],
            vec![76, 47, 83, 55, 33, 5, 10, 20, 28],
            vec![2, 63, 67, 40, 57],
            vec![14, 34],
        ];
        let source = Source::new(data).with_chunk_size(20);
        let mut save_buffer = BufferPreallocated::new(40).with_name("save buffer");
        let mut work_buffer = BufferPreallocated::new(40).with_name("work buffer");

        assert_eq!(
            expected,
            parse(source, &mut save_buffer, &mut work_buffer).unwrap()
        );
    }

    #[test_pretty_log::test]
    fn test_yield_source_error() {
        let data = b"(1,2,3,(4,5,6),7,8,9)(61,36,16,20,7)(62))(45,18,47,77,a,40,59,21)(21,6)<.(39,4,3)(76,47,83,55,33,5,10,20,28)R(2,63,67,40,57))(14,34)(";
        let expected = vec![
            vec![4, 5, 6],
            vec![61, 36, 16, 20, 7],
            vec![62],
            vec![21, 6],
            vec![39, 4, 3],
            vec![76, 47, 83, 55, 33, 5, 10, 20, 28],
            vec![2, 63, 67, 40, 57],
            vec![14, 34],
        ];
        let source = Source::new(data).with_chunk_size(20);
        let mut save_buffer = BufferPreallocated::new(40).with_name("save buffer");
        let mut work_buffer = BufferPreallocated::new(40).with_name("work buffer");
        let parser = crate::parsers::parse_data;
        let group_start = StartGroup {
            parser: start_group_parenthesis,
            start_character: b"(",
        };
        let stream = StreamParser::new(
            source,
            &mut save_buffer,
            &mut work_buffer,
            parser,
            group_start,
        );

        let result = stream.flatten().collect::<Vec<Vec<u8>>>();

        // let y = &x
        //     .map(|ref x| format!("{x:?}"))
        //     .map_err(|ref err| err.map_input(|input| { debug!(&input) }.to_string()));

        assert_eq!(expected, result);
    }

    #[test_pretty_log::test]
    fn test_various_data() {
        let data = b"%%255%%ertftuijo%%4%%";
        let source = Source::new(data).with_chunk_size(6);
        let mut save_buffer = BufferPreallocated::new(40).with_name("save buffer");
        let mut work_buffer = BufferPreallocated::new(40).with_name("work buffer");
        let expected = vec![vec![255], vec![4]];
        let parser = parse_percentage;
        let group_start = StartGroup {
            parser: start_group_percentage,
            start_character: b"%",
        };
        let stream = StreamParser::new(
            source,
            &mut save_buffer,
            &mut work_buffer,
            parser,
            group_start,
        );

        let result = stream.flatten().collect::<Vec<Vec<u8>>>();

        assert_eq!(expected, result);
    }

    #[test_pretty_log::test]
    fn test_data_u32() {
        let data = b"%%25565456%%ertftuijo%%4%%";
        let source = Source::new(data).with_chunk_size(6);
        let mut save_buffer = BufferPreallocated::new(40).with_name("save buffer");
        let mut work_buffer = BufferPreallocated::new(40).with_name("work buffer");
        let expected = vec![vec![25565456], vec![4]];
        let parser = parse_data_u32;
        let group_start = StartGroup {
            parser: start_group_percentage,
            start_character: b"%",
        };
        let stream = StreamParser::new(
            source,
            &mut save_buffer,
            &mut work_buffer,
            parser,
            group_start,
        );

        let result = stream.flatten().collect::<Vec<Vec<u32>>>();

        assert_eq!(expected, result);
    }

    #[test_pretty_log::test]
    fn test_unicode_data() {
        let unicode = std::str::from_utf8("你2,3,4好一二三四五六七八九你2,4好".as_bytes()).unwrap();

        let data = unicode.as_bytes();
        let source = Source::new(data).with_chunk_size(6);
        let mut save_buffer = BufferPreallocated::new(40).with_name("save buffer");
        let mut work_buffer = BufferPreallocated::new(40).with_name("work buffer");
        let expected = vec![vec![2, 3, 4], vec![2, 4]];
        let parser = parse_chinese;
        let group_start = StartGroup {
            parser: start_group_chinese,
            start_character: &"你".as_bytes()[0..1],
        };
        let stream = StreamParser::new(
            source,
            &mut save_buffer,
            &mut work_buffer,
            parser,
            group_start,
        );

        let result = stream.flatten().collect::<Vec<Vec<u8>>>();

        assert_eq!(expected, result);
    }
}
